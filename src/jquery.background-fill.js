(function ( $ ) {
 
  $.fn.backgroundFill = function( options ) {
	  
    var 
    	elements = this,
	    settings = $.extend({
  	    edge:   0.02,
  	    once:   true,
		    debug:  false
	    }, options )
	    ;
        
		elements
      .each(function() {
        
        var
          el = $(this),
          fill,
          imgUrl,
          img;
        
        if ( ( ! el.data('backgroundFill.initialized') && settings.once ) || ! settings.once )
        {
                    
          if ( el.prop('tagName').toLowerCase() === 'img' )
          {
            // el is an image
            
            fill    = el.parent();
            imgUrl  = el.src;
            img     = el;
            
          }
          else if ( el.css('backgroundImage') !== 'none' )
          {
            // el has background image
            
            fill    = el;
            imgUrl  = el.css('backgroundImage').replace(/(url\(|\)|'|")/gi, '');
            img     = new Image();
            img.src = imgUrl;
            
          }
          else
          {
            // not sure what to do
            
            console.log('[jQuery backgroundFill] Can\'t find an image! This must be applied to an <img> element or an element with a background image');
            return;
            
          }
          
          $(img)
            .one('load', function() {
              
              var 
                img     = this,
                canvas  = document.createElement('canvas'),
                edge    = Math.max( 0, Math.min( 1, settings.edge ) ),
                points  = [ { x: edge, y: edge }, { x: 1 - edge, y: edge }, { x: 1 - edge, y: 1 - edge }, { x: edge, y: 1 - edge } ],
                r = 0, g = 0, b = 0;
              
              canvas.width  = img.width;
              canvas.height = img.height;
              
              canvas
                .getContext('2d')
                .drawImage( img, 0, 0, img.width, img.height );  
              
              $.each(points, function(index, p) {
                var 
                  rgb = canvas.getContext('2d').getImageData(Math.floor(p.x * img.width), Math.floor(p.y * img.height), 1, 1).data;
                r += rgb[0];
                g += rgb[1];
                b += rgb[2];
              });
              
              el
                .data('backgroundFill.initialized', true);
              
              fill
                .css('backgroundColor', 'rgb(' + ( r / points.length ) + ',' + ( g / points.length ) + ',' +  ( b / points.length ) + ')');          
              
            })
            .each(function() {
              if( this.complete ) 
              {
                $(this).trigger('load');
              }
            });
          
        }
        
      });  			
	    
    return this;
  };
 
}( jQuery ));

