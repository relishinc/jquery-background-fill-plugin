# Background Fill Plugin

Take the background colour of an image and extend it outwards to fill it's container. Works on <img> elements or on elements with background images. Ideal for images with a solid background colour (like logos).

### Installation

Install via Bower

```bash
bower install https://bitbucket.org/relishinc/jquery-background-fill-plugin.git --save
```

### Usage

jQuery plugin can be used like so:

```javascript
$('.some-selector')
	.backgroundFill(options)
```

### Options

```javascript
{
  edge: 0.02,  // how close to the edge of the image the background colour should be grabbed (default is 0.02, or 2%)
  once: true   // if it will only be run once on the element
}
```